# POSIX Wrapper for cc/c99/c2x Tools

## POSIX 202x

Adds new options to c99

* -G: shared libraries
* -B: dynamic executables
* -R: setting rpath

To quote from the standard:

> −B mode : If *mode* is **dynamic**, produce a dynamically linked executable file. If the **−B** option is present with **−c**, **−E**, or **−G**, the result is unspecified.

For shared libraries:

> −G : Create a shared library or create object files suitable for inclusion in such a shared library. Compilations shall be performed in a manner suitable for the creation of shared libraries (for example, by producing position-independent code).
>
> If **−c** is also specified, create object files suitable for inclusion in a shared library.
>
> If **−c** is not specified, create a shared library. In this case the application shall ensure that the file named by the **−o** *outfile* option-argument includes an element named **so** or an implementation-defined element denoting a shared library, where elements in the last component of *outfile* are separated by `<period>` characters, for example **libx.so.1**; if no **−o** option is included in the options or the file named by the **−o** *outfile* option does not contain an element named **so** or an implementation-defined element denoting a shared library, the result is unspecified. If a *pathname* operand or **−l** option-argument names a shared library and that shared library defines an object used by the library being created, it shall become a dependency of the created shared library.
>
> If the **−G** option is present with **−B** or **−E**, the result is unspecified.

For the rpath:

> −R directory : If the object file format supports it, specify a directory to be searched for shared libraries when an executable file or shared library being created by *c99* is subsequently executed, or loaded using *dlopen()*. If *directory* contains any `<colon>` or `<dollar-sign>` characters, the behavior is unspecified. If an implementation provides a means for setting a default load time search location or locations, the **−R** option shall take precedence.
>
> The directory named by *directory* shall not be searched by a process performing dynamic loading if either of the following are true:
>
> * The real and effective user IDs of that process are different and the directory has write permission for a user ID outside the set of the effective user ID of that process and any implementation-specific user IDs used for directories containing system libraries.
> * The real and effective group IDs of that process are different and the directory has write permission for group IDs other than the effective group ID of that process.
>
> Directories named in **−R** options shall be searched in the order specified, before the default system library locations are searched.
>
> If a directory specified by a **−R** option contains files with names starting with any of the strings `"libc."`, `"libl."`, `"libpthread."`, `"libm."`, `"librt."`, `"libxnet."`, or `"liby."`, the result is unspecified.
>
> If the **−R** option is present with **−c** or **−E**, the result is unspecified.

And finally:

> Multiple instances of the **−D**, **−I**, **−L**, **−l**, **−R**, and **−U** options can be specified.
